package metadata_test

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/mobsf/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

func TestReportScanner(t *testing.T) {
	want := report.ScannerDetails{
		ID:      "mobsf",
		Name:    "MobSF",
		Version: metadata.ScannerVersion,
		Vendor: report.Vendor{
			Name: "MobSF",
		},
		URL: "https://github.com/MobSF/Mobile-Security-Framework-MobSF",
	}
	got := metadata.ReportScanner

	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
