package main

import (
	"archive/zip"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/textproto"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/mobsf/v2/plugin"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

const (
	mobsfDefaultAddr   = "http://localhost:8000/api/v1"
	uploadPath         = "/upload"
	scanPath           = "/scan"
	authHeader         = "Authorization"
	contentTypeHeader  = "Content-Type"
	urlEncodedFormType = "application/x-www-form-urlencoded"
	excludeDirs        = "exclude-dirs"
)

var (
	apiKey           = os.Getenv("MOBSF_API_KEY")
	mobsfAddr        = os.Getenv("MOBSF_ADDR")
	wantedExtensions = []string{
		".xml",
		".pbxproj",
		".plist",
		".java",
		".kt",
		".properties",
		".jar",
		".swift",
		".m",
		".h",
		".c",
	}
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringSliceFlag{
			Name:    excludeDirs,
			Usage:   "List of directories to exclude from scan",
			EnvVars: []string{"SAST_MOBSF_EXCLUDE_DIRS"},
		},
	}
}

func getBaseURL() string {
	if mobsfAddr != "" {
		return mobsfAddr
	}

	return mobsfDefaultAddr
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	excludedDirs := c.StringSlice(excludeDirs)
	log.Debugf("Excluded directories: %s", excludedDirs)

	// MobSF expects AndroidManifest.xml to be at either the project root
	// or app/src/main/AndroidManifest.xml. If it's not at one of those locations,
	// then we will copy it over so that analysis may continue.
	// https://github.com/MobSF/Mobile-Security-Framework-MobSF/issues/1553
	if plugin.AnalysisType() == plugin.AnalysisTypeAndroid {
		if err := prepWorkspace(plugin.ManifestPath(), path); err != nil {
			return nil, fmt.Errorf("could not copy Android manifest: %w", err)
		}
	}

	archive, err := createArchive(path, excludedDirs)
	if err != nil {
		return nil, fmt.Errorf("could not create zip archive of project: %w", err)
	}

	info, err := upload(archive)
	if err != nil {
		return nil, fmt.Errorf("failed to upload archive to mobsf: %w", err)
	}

	log.Infof("Starting scan. Type: %s, Upload Hash: %s", info.Type, info.Hash)

	report, err := scan(info)
	if err != nil {
		return nil, fmt.Errorf("scan failed: %w", err)
	}

	return ioutil.NopCloser(report), nil
}

func prepWorkspace(manifestPath, analysisRoot string) error {
	// If it's already at one of the expected locations, then no action is required.
	if plugin.IsMobsfFriendlyManifestPath(manifestPath, analysisRoot) {
		return nil
	}

	destPath := path.Join(analysisRoot, plugin.Manifest)
	log.Infof("Manifest is at %s. Copying it to %s.", manifestPath, destPath)
	dest, err := os.Create(destPath)
	if err != nil {
		return fmt.Errorf("failed to open destination file for writing: %w", err)
	}
	defer dest.Close()

	src, err := os.Open(manifestPath)
	if err != nil {
		return fmt.Errorf("failed to open source file for reading: %w", err)
	}
	defer src.Close()

	if _, err := io.Copy(dest, src); err != nil {
		return fmt.Errorf("failed to copy file contents: %w", err)
	}

	// MobSF also checks that src/ and res/values/ directories exist.
	if err := createNoopDir(path.Join(analysisRoot, "src")); err != nil {
		return err
	}

	if err := createNoopDir(path.Join(analysisRoot, "res", "values")); err != nil {
		return err
	}

	return nil
}

func createNoopDir(dir string) error {
	if err := os.MkdirAll(dir, 0750); err != nil {
		return fmt.Errorf("could not create dummy directory: %w", err)
	}

	f, err := os.Create(path.Join(dir, "noop.xml"))
	if err != nil {
		return fmt.Errorf("could not create dummy file: %w", err)
	}
	f.WriteString("<noop/>")
	f.Close()
	return nil
}

func createArchive(projectDir string, excludedDirs []string) (io.Reader, error) {
	log.Debugf("Creating zip starting in dir: %s", projectDir)
	buff := new(bytes.Buffer)
	w := zip.NewWriter(buff)
	defer w.Close()

	start, _ := os.Getwd()

	if err := os.Chdir(projectDir); err != nil {
		return nil, fmt.Errorf("could not change into directory %q: %w", projectDir, err)
	}

	pwd, _ := os.Getwd()
	log.Debugf("Current working directory: %s", pwd)

	// For now, we will just zip all the the files related to either iOS or Android.
	// At some point, we may wish to collect separate groups of file extensions
	// based on the platform being scanned.
	// To be addressed with https://gitlab.com/gitlab-org/gitlab/-/issues/263474
	err := filepath.Walk(".", fileWalker(w, excludedDirs))

	if err != nil {
		return nil, fmt.Errorf("error creating zip archive: %w", err)
	}

	// Change back to the start dir before we exit the function.
	if err := os.Chdir(start); err != nil {
		// Shouldn't matter if this errors since the remaining processes (convert)
		// pay no mind to the working directory
	}

	return buff, nil
}

func isWantedExtension(ext string) bool {
	for _, want := range wantedExtensions {
		if ext == want {
			return true
		}
	}

	return false
}

func fileWalker(w *zip.Writer, excludedDirs []string) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Warnf(err.Error())
			return nil
		}

		name := info.Name()
		if strings.HasPrefix(name, ".") && name != "." {
			log.Tracef("%s is a dotfile, skipping", name)
			if info.IsDir() {
				return filepath.SkipDir
			}
			return nil
		}

		if info.IsDir() {
			for _, dir := range excludedDirs {
				if match, _ := filepath.Match(dir, path); match {
					log.Infof("Excluding directory: %s", dir)
					return filepath.SkipDir
				}
			}
			log.Tracef("%s is a directory, skipping", name)
			return nil
		}

		if !isWantedExtension(filepath.Ext(name)) {
			log.Tracef("%s does not have a wanted file extension, skipping", name)
			return nil
		}

		writer, err := w.Create(path)
		if err != nil {
			log.Warnf("Could not create zip writer: %s", err.Error())
			return nil
		}

		f, err := os.Open(filepath.Clean(path))
		if err != nil {
			log.Warnf("Could not open %q: %s", path, err.Error())
			return nil
		}

		if _, err := io.Copy(writer, f); err != nil {
			log.Warnf("Failed to write to archive: %s", err.Error())
			return nil
		}

		log.Infof("Zipping: %s", path)

		return nil
	}
}

func newRequest(method, url string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, fmt.Errorf("error creating http request: %w", err)
	}
	req.Header.Set(authHeader, apiKey)
	return req, nil
}

func upload(archive io.Reader) (*scanInfo, error) {
	body, contentType := createMultipart(archive)
	req, err := newRequest(http.MethodPost, getBaseURL()+uploadPath, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set(contentTypeHeader, contentType)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.WithFields(log.Fields{
			"url":   uploadPath,
			"error": err,
		}).Debug("error requesting scan")
		return nil, fmt.Errorf("error requesting scan: %w", err)
	}
	defer resp.Body.Close()
	return parseScanInfo(resp.Body)
}

// scan blocks until the scan is completed and then returns the scan
// report as raw json.
func scan(info *scanInfo) (io.Reader, error) {
	body := strings.NewReader(info.ToData().Encode())
	req, err := newRequest(http.MethodPost, getBaseURL()+scanPath, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set(contentTypeHeader, urlEncodedFormType)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("scan request failed: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		b, _ := ioutil.ReadAll(resp.Body)
		return nil, fmt.Errorf("scan responded with unexpected status code (%d): %s", resp.StatusCode, string(b))
	}

	// Reading resp.Body streams the bytes from the HTTP connection.
	// We copy the response into a buffer so that we may close the connection.
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read response body: %w", err)
	}

	return bytes.NewBuffer(b), nil
}

func createMultipart(archive io.Reader) (*bytes.Buffer, string) {
	h := make(textproto.MIMEHeader)
	h.Set("Content-Disposition", `form-data; name="file"; filename="analyze.zip"`)
	h.Set("Content-Type", "application/octet-stream")
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	w, _ := writer.CreatePart(h)
	// Should only return an error if the archive has been closed.
	if _, err := io.Copy(w, archive); err != nil {
		log.Warnf("Failed to write %q to archive: %s", w, err.Error())
	}

	if err := writer.Close(); err != nil {
		log.Warnf("Failed to close %q to archive: %s", w, err.Error())
	}

	return body, writer.FormDataContentType()
}

type scanInfo struct {
	Type     string `json:"scan_type"`
	FileName string `json:"file_name"`
	Hash     string `json:"hash"`
}

func parseScanInfo(body io.ReadCloser) (*scanInfo, error) {
	info := new(scanInfo)
	if err := json.NewDecoder(body).Decode(info); err != nil {
		log.WithField("error", err).Debug("failed to unmarshal scan response json")
		return nil, fmt.Errorf("failed to unmarshal scan response json: %w", err)
	}

	return info, nil
}

func (s *scanInfo) ToData() url.Values {
	values := url.Values{}
	values.Set("scan_type", s.Type)
	values.Set("file_name", s.FileName)
	values.Set("hash", s.Hash)
	return values
}
