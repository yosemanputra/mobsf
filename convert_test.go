package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConvert(t *testing.T) {
	wantBytes, err := ioutil.ReadFile("./testdata/expect/gl-sast-report-android.json")
	if err != nil {
		t.Fatal(err)
	}
	want := string(wantBytes)
	in, err := os.Open("./testdata/android_report.json")
	defer in.Close()
	if err != nil {
		t.Fatal(err)
	}

	got, err := convert(in, "")
	gotReportBytes, err := json.MarshalIndent(got, "", "  ")
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, string(want), string(gotReportBytes), "should be equal")
}
