package plugin

import (
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

var (
	manifestPath string
	analysisType string
)

const (
	// AnalysisTypeIOS is the identifier for iOS analysis
	AnalysisTypeIOS = "ios"
	// AnalysisTypeAndroid is the identifier for Android analysis
	AnalysisTypeAndroid = "android"
	// Manifest is the file name of the Android manifest
	Manifest = "AndroidManifest.xml"
)

// Match checks if the file is AndroidManifest.xml or *.xcodeproj
func Match(path string, info os.FileInfo) (bool, error) {
	filename := info.Name()
	if filepath.Ext(filename) == ".xcodeproj" {
		analysisType = AnalysisTypeIOS
		return true, nil
	}

	if filename == Manifest {
		analysisType = AnalysisTypeAndroid
		manifestPath = path
		return true, nil
	}

	return false, nil
}

// AnalysisType returns whether the type of analysis being performed is iOS or Android.
// It must be called after the Match stage.
func AnalysisType() string {
	return analysisType
}

// ManifestPath returns the path to the Android manifest if AnalysisType is AnalysisTypeAndroid.
// It must be called after the Match stage.
func ManifestPath() string {
	return manifestPath
}

// IsMobsfFriendlyManifestPath returns true if the given path is "AndroidManifest.xml" or "app/src/main/AndroidManifest.xml"
func IsMobsfFriendlyManifestPath(path, analysisRoot string) bool {
	if path == filepath.Join(analysisRoot, Manifest) {
		return true
	}

	if path == filepath.Join(analysisRoot, "app", "src", "main", Manifest) {
		return true
	}

	return false
}

func init() {
	plugin.Register("mobsf", Match)
}
