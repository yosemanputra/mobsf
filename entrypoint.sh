#!/usr/bin/env bash

export MOBSF_API_KEY="key"
export MOBSF_URL="localhost:8000"

cd /root/Mobile-Security-Framework-MobSF
python3 manage.py makemigrations 2&>> manage.out && \
python3 manage.py makemigrations StaticAnalyzer 2&>> manage.out && \
python3 manage.py migrate 2&>> manage.out
gunicorn -b 127.0.0.1:8000 \
  "mobsf.MobSF.wsgi:application" \
  --workers=1 \
  --threads=10 \
  --timeout=1800 \
  --log-file /tmp/gunicorn.log &

# Wait for MobSF for up to 10 seconds
for i in $(seq 11); do
  grep -qs "Booting worker with pid:" /tmp/gunicorn.log
  grep_match=$?

  if [ $grep_match -eq 0 ]; then
    break
  fi

  if [ $i -eq 11 ]; then
    echo "COULDN'T START MOBSF"
    exit 1
  fi

  sleep 1
done

# Execute docker command
exec "$@"
